package io.vertx.guides.wiki;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.rxjava.core.AbstractVerticle;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        final String dbVerticle = "io.vertx.guides.wiki.database.WikiDatabaseVerticle";
        final String httpVerticle = "io.vertx.guides.wiki.http.HttpServerVerticle";
        
        vertx.rxDeployVerticle(dbVerticle).flatMap(id -> 
            vertx.rxDeployVerticle(httpVerticle, new DeploymentOptions().setInstances(2))
        )
        .subscribe(id -> startFuture.complete(), startFuture::fail);
    }

}